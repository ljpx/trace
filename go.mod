module gitlab.com/ljpx/trace

go 1.12

require (
	github.com/stretchr/testify v1.4.0
	gitlab.com/ljpx/id v1.0.0
)
