package trace

// Redactable defines the methods that a redactable model must implement. Redact
// should return a copy of the original value with redactable fields redacted.
type Redactable interface {
	Redact() interface{}
}
