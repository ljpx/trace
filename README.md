![](icon.png)

# trace

Package `trace` provides a number of primitives used for tracking and logging
information for visibility and debugging.

## Usage Example

The main usage of the `trace` package is to enable easy logging for transient
instances.  We can implement `trace.Traceable` like so:

```go
type CreateUserCommand struct {
    trace.CorrelatableBase

    FirstName string
    LastName string
}

func (c *CreateUserCommand) Identify() string {
    return "CreateUserCommand"
}

func (c *CreateUserCommand) Describe() string {
    return fmt.Sprintf("Create a new user with FirstName %q and LastName %q", c.FirstName, c.LastName)
}

func (c *CreateUserCommand) Redact() interface{} {
    cp := &c
    cp.LastName = "(Redacted)"
    return &cp
}
```

Instances of this struct can now be treated as a `trace.Traceable`.  They can be
correlated with other `trace.Traceable` instances (or `trace.Correlatable`) by
calling `x.CorrelateWith(y)`.
