package trace

// Traceable defines the methods that any traceable type must implement.  A type
// is traceable if it is Correlatable, Describable, Redactable and Identifiable.
type Traceable interface {
	Correlatable
	Redactable
	Describable
	Identifiable
}
