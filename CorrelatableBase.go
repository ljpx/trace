package trace

import "gitlab.com/ljpx/id"

// CorrelatableBase is an implementation of Correlatable that can be embedded
// easily into struct definitions.
type CorrelatableBase struct {
	Correlations map[string]id.ID `json:"correlations,omitempty"`
}

var _ Correlatable = &CorrelatableBase{}

// AddCorrelation adds a Correlation to the struct.
func (c *CorrelatableBase) AddCorrelation(name string, correlationID id.ID) {
	c.ensureMapExists()
	c.Correlations[name] = correlationID
}

// RemoveCorrelation removes the Correlation with the provided name.
func (c *CorrelatableBase) RemoveCorrelation(name string) {
	c.ensureMapExists()
	delete(c.Correlations, name)
}

// HasCorrelation returns true if the provided name exists in the set of
// correlations.
func (c *CorrelatableBase) HasCorrelation(name string) bool {
	_, ok := c.GetCorrelation(name)
	return ok
}

// GetCorrelation returns the correlationID for the provided name, if it exists.
func (c *CorrelatableBase) GetCorrelation(name string) (id.ID, bool) {
	c.ensureMapExists()

	correlationID, ok := c.Correlations[name]
	return correlationID, ok
}

// GetCorrelations returns the set of correlationIDs for the given instance.
// This can return nil.
func (c *CorrelatableBase) GetCorrelations() map[string]id.ID {
	return c.Correlations
}

// EmptyCorrelations overwrites the set of correlation IDs with a new, empty
// map.
func (c *CorrelatableBase) EmptyCorrelations() {
	c.Correlations = nil
}

// CorrelateWith ensures the caller has all the same correlations as the
// provided Correlatable.  It will not overwrite existing correlations.
func (c *CorrelatableBase) CorrelateWith(other Correlatable) {
	for name, correlationID := range other.GetCorrelations() {
		if !c.HasCorrelation(name) {
			c.AddCorrelation(name, correlationID)
		}
	}
}

func (c *CorrelatableBase) ensureMapExists() {
	if c.Correlations == nil {
		c.Correlations = make(map[string]id.ID)
	}
}
