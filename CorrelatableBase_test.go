package trace

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ljpx/id"
)

func TestCorrelatableBaseAdd(t *testing.T) {
	// Arrange.
	c := &CorrelatableBase{}
	ID1 := id.New()
	require.False(t, c.HasCorrelation("test"))

	// Act.
	c.AddCorrelation("test", ID1)

	// Assert.
	ID2, ok := c.GetCorrelation("test")
	require.True(t, c.HasCorrelation("test"))
	require.True(t, ok)
	require.Equal(t, ID1, ID2)
}

func TestCorrelatableBaseAddExisting(t *testing.T) {
	// Arrange.
	c := &CorrelatableBase{}
	ID1 := id.New()
	ID2 := id.New()

	// Act.
	c.AddCorrelation("test", ID1)
	c.AddCorrelation("test", ID2)

	// Assert.
	ID3, ok := c.GetCorrelation("test")
	require.True(t, c.HasCorrelation("test"))
	require.True(t, ok)
	require.Equal(t, ID2, ID3)
}

func TestCorrelatableBaseRemove(t *testing.T) {
	// Arrange.
	c := &CorrelatableBase{}
	ID1 := id.New()
	c.AddCorrelation("test", ID1)

	// Act.
	c.RemoveCorrelation("test")

	// Assert.
	ID2, ok := c.GetCorrelation("test")
	require.False(t, c.HasCorrelation("test"))
	require.False(t, ok)
	require.Equal(t, id.Empty, ID2)
}

func TestCorrelatableBaseClean(t *testing.T) {
	// Arrange.
	c := &CorrelatableBase{}
	ID1 := id.New()

	c.AddCorrelation("test", ID1)

	// Act.
	c.EmptyCorrelations()

	// Assert.
	require.Len(t, c.GetCorrelations(), 0)
}

func TestCorrelatableBaseCorrelateWithEmpty(t *testing.T) {
	// Arrange.
	c1 := &CorrelatableBase{}
	c2 := &CorrelatableBase{}

	// Act.
	c1.CorrelateWith(c2)

	// Assert.
	require.Len(t, c1.GetCorrelations(), 0)
	require.Len(t, c2.GetCorrelations(), 0)
}

func TestCorrelatableBaseCorrelateWith(t *testing.T) {
	// Arrange.
	c1 := &CorrelatableBase{}
	c2 := &CorrelatableBase{}

	ID1 := id.New()
	ID2 := id.New()
	ID3 := id.New()
	ID3A := id.New()
	ID4 := id.New()
	ID5 := id.New()

	c1.AddCorrelation("ID1", ID1)
	c1.AddCorrelation("ID2", ID2)
	c1.AddCorrelation("ID3", ID3)

	c2.AddCorrelation("ID3", ID3A)
	c2.AddCorrelation("ID4", ID4)
	c2.AddCorrelation("ID5", ID5)

	// Act.
	c1.CorrelateWith(c2)

	// Assert.
	require.Len(t, c1.GetCorrelations(), 5)
	require.Len(t, c2.GetCorrelations(), 3)
	require.Equal(t, map[string]id.ID{"ID1": ID1, "ID2": ID2, "ID3": ID3, "ID4": ID4, "ID5": ID5}, c1.GetCorrelations())
}

func TestCorrelatableBaseJSONEmpty(t *testing.T) {
	// Arrange.
	demo := &DemoModel{}

	// Act.
	rawJSON, err := json.Marshal(demo)
	require.Nil(t, err)

	json := string(rawJSON)

	// Assert.
	require.Equal(t, `{}`, json)
}

func TestCorrelatableBaseJSON(t *testing.T) {
	// Arrange.
	demo := &DemoModel{}
	ID := id.New()
	demo.AddCorrelation("test", ID)

	// Act.
	rawJSON, err := json.Marshal(demo)
	require.Nil(t, err)

	json := string(rawJSON)

	// Assert.
	require.Equal(t, fmt.Sprintf(`{"correlations":{"test":"%v"}}`, ID), json)
}

// -----------------------------------------------------------------------------
type DemoModel struct {
	CorrelatableBase
}
