package trace

import "gitlab.com/ljpx/id"

// Correlatable defines the methods that any correlatable type must implement.
// A correlatable type has set of CorrelationIDs which can be easily passed on
// or created using the CorrelateWith method.
type Correlatable interface {
	AddCorrelation(name string, correlationID id.ID)
	RemoveCorrelation(name string)
	HasCorrelation(name string) bool
	GetCorrelation(name string) (id.ID, bool)
	GetCorrelations() map[string]id.ID
	EmptyCorrelations()

	CorrelateWith(other Correlatable)
}
