package trace

// Describable defines the methods that any describable type must implement.  It
// is used mainly to summarize the contents and/or purpose of the implementing
// struct.
type Describable interface {
	Describe() string
}
