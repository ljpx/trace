package trace

// Identifiable defines the methods that any identifiable type must implement.
// It should be used to unique identify the type, not the instance.
type Identifiable interface {
	Identify() string
}
